/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jflexproy2;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author kevin
 */
public class Jflexproy2 {

   public static void main(String[] args) throws Exception {
        pantalla_jflex.main(args);
        //se quita
        /*
        try{
            generarLexer();
        }catch(Exception e){
            throw new Exception("Error al generar Archivo Lexer");
        }
        */
   }
    
    public static void generarLexer() throws Exception{
        // Ruta del archivo donde se encuentra el archivo lex.flex
        String[] ruta = {"C:/Users/kevin/OneDrive/Documents/NetBeansProjects/jflexProy2/src/jflexproy2/lex.flex"};
        try {
            jflex.Main.generate(ruta);
        }catch(Exception e){
            throw new Exception("Error al crear Archivo Lexer.java ");
        }
    }
    
    /*
    public static void main(String[] args) throws Exception {
       System.out.println("Generando Archivos Lexers");
        try{
           generar();
        }catch(Exception e){
           throw new Exception("Error al generar Archivo Lexer");
          }
     }
    */

     public static void generar() throws IOException, Exception{
        String rutaProy = "C:/Users/kevin/OneDrive/Documents/NetBeansProjects/jflexProy2";
        String[] ruta1 = {rutaProy+"/src/jflexproy2/lex.flex"};
        String[] ruta2 = {rutaProy+"/src/jflexproy2/LexerCup.flex"};
        String[] rutaS = {"-parser", "Sintax", rutaProy+"/src/jflexproy2/Sintax.cup"};

        jflex.Main.generate(ruta1);       
        jflex.Main.generate(ruta2);
        java_cup.Main.main(rutaS);
        
        Path rutaSym = Paths.get(rutaProy+"/src/jflexproy2/sym.java");
        if (Files.exists(rutaSym)) {
            Files.delete(rutaSym);
        }
        Files.move(
                Paths.get(rutaProy+"/sym.java"), 
                Paths.get(rutaProy+"/src/jflexproy2/sym.java")
        );
        Path rutaSin = Paths.get(rutaProy+"/src/jflexproy2/Sintax.java");
        if (Files.exists(rutaSin)) {
            Files.delete(rutaSin);
        }
        Files.move(
                Paths.get(rutaProy+"/Sintax.java"), 
                Paths.get(rutaProy+"/src/jflexproy2/Sintax.java")
        );
    }}
